import data from "@/services/data";
import Scorer from "@/services/scorers/Scorer";
import Locations from "@/config/Locations";

export default {

  state: {

    scorer: new Scorer(),

    //Adding default scores of 0 for all questions
    shredders: data.map(s => {
      s.scores = {
        location: 0.0, //Where will the shredder be used
        frequency: 0.0, // How often will you shred
        capacity: 0.0, // How much material do you plan to shred per day
        users: 0.0, //How many people will use the shredder
        cleanTech: 0.0, //Would you like a fine dust filter system
        security: 0.0, //What level of security do you require

      };

      s.overallScore = 0.0;

      return s;
    }),

    //User answers to questions. All unanswered questions = null and won't be factored in scoring
    answers: {
      location: null,
      frequency: null,
      capacity: null,
      users: null,
      cleanTech: null,
      security: null
    }

  },

  getters: {
    
    answers(state) {
      return state.answers;
    },

    results(state) {
      const sortedByScore = state.shredders.sort(function (a,b) {
        if (a.overallScore > b.overallScore)
          return -1;
        else if (a.overallScore < b.overallScore)
          return 1;
        else
          return 0;
      });


      const topFive = sortedByScore.slice(0, 3);
      topFive.forEach(s => console.log(s.model, s.category, s.users, s.cleantec, s.security, JSON.stringify(s.scores), s.overallScore));
      return sortedByScore;
    },
  },


  mutations: {
    
    setLocation(state, location) {
      state.answers.location = location;
      this.commit('recalcLocationScore');
      this.commit('recalcOverallScore');
    },

    setFrequency(state, freq) {
      state.answers.frequency = freq;
      //not recalcing scores cuz only 'daily' frequencies are in the data right now
    },

    setCapacity(state, cap) {
      state.answers.capacity = cap;
      this.commit('recalcCapacityScore');
      this.commit('recalcOverallScore');
    },

    setUsers(state, users) {
      state.answers.users = users;
      this.commit('recalcUsersScore');
      this.commit('recalcOverallScore');
    },

    setCleanTech(state, cleanTech) {
      state.answers.cleanTech = cleanTech;
      this.commit('recalcCleanTechScore');
      this.commit('recalcOverallScore');
    },

    setSecurity(state, security) {
      state.answers.security = security;
      this.commit('recalcSecurityScore');
      this.commit('recalcOverallScore');
    },

    //Recalculate the location score, whenever the answer for 'location' question changes
    recalcLocationScore(state) {
      const answer = state.answers.location;

      state.shredders = state.shredders.map(s => {
        s.scores.location = state.scorer.locationScore(s, answer);
        return s;
      });
    },

    recalcCapacityScore(state) {
      const answer = state.answers.capacity;

      state.shredders = state.shredders.map(s => {
        s.scores.capacity = state.scorer.capacityScore(s, answer);
        return s;
      });
    },



    recalcUsersScore(state) {
      const answer = state.answers.users;

      state.shredders = state.shredders.map(s => {
        s.scores.users = state.scorer.usersScore(s, answer);
        return s;
      });
    },


    recalcCleanTechScore(state) {
      const answer = state.answers.cleanTech;

      state.shredders = state.shredders.map(s => {
        s.scores.cleanTech = state.scorer.cleanTechScore(s, answer);
        return s;
      });
    },


    recalcSecurityScore(state) {
      const answer = state.answers.security;

      state.shredders = state.shredders.map(s => {
        s.scores.security = state.scorer.securityScore(s, answer);
        return s;
      });
    },


    recalcOverallScore(state) {
      state.shredders = state.shredders.map(s => {
        s.overallScore = state.scorer.overallScore(s);
        return s;
      })
    },

  },

  actions: {

  }
}
