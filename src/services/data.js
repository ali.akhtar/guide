export default [
  {
    model: '20390',
    category: 'High Capacity',
    frequency: 'daily',
    capacity: '55 / 60',
    cleantec: 'no',
    security: 'P-2',
    users: '5+'
  },
  {
    model: '20392',
    category: 'High Capacity',
    frequency: 'daily',
    capacity: '26 / 28',
    cleantec: 'no',
    security: 'P-5',
    users: '5+'
  },
  {
    model: '20394',
    category: 'High Security',
    frequency: 'daily',
    capacity: '9 / 11',
    cleantec: 'no',
    security: 'P-7',
    users: '5+'
  },
  {
    model: '20396',
    category: 'High Capacity',
    frequency: 'daily',
    capacity: '38 / 42',
    cleantec: 'no',
    security: 'P-4',
    users: '5+'
  },
  {
    model: '40330',
    category: 'Small Office',
    frequency: 'daily',
    capacity: '6 / 8',
    cleantec: 'no',
    security: 'P-6',
    users: '3'
  },
  {
    model: '40334',
    category: 'High Security',
    frequency: 'daily',
    capacity: '5 / 7',
    cleantec: 'no',
    security: 'P-7',
    users: '5+'
  },
  {
    model: '40406',
    category: 'Office',
    frequency: 'daily',
    capacity: '30 / 34',
    cleantec: 'no',
    security: 'P-2',
    users: '5'
  },
  {
    model: '40430',
    category: 'Office',
    frequency: 'daily',
    capacity: '9 / 11',
    cleantec: 'no',
    security: 'P-6',
    users: '5'
  },
  {
    model: '40434',
    category: 'High Security',
    frequency: 'daily',
    capacity: '8 / 10',
    cleantec: 'no',
    security: 'P-7',
    users: '5+'
  },
  {
    model: '40530',
    category: 'Department',
    frequency: 'daily',
    capacity: '10 / 12',
    cleantec: 'no',
    security: 'P-6',
    users: '5+'
  },
  {
    model: '40534',
    category: 'High Security',
    frequency: 'daily',
    capacity: '8 / 10',
    cleantec: 'no',
    security: 'P-7',
    users: '5+'
  },
  {
    model: '40606',
    category: 'Department',
    frequency: 'daily',
    capacity: '36 / 40',
    cleantec: 'no',
    security: 'P-2',
    users: '5+'
  },
  {
    model: '41334',
    category: 'High Security',
    frequency: 'daily',
    capacity: '5 / 7',
    cleantec: 'yes',
    security: 'P-7',
    users: '5+'
  },
  {
    model: '41434',
    category: 'High Security',
    frequency: 'daily',
    capacity: '8 / 10',
    cleantec: 'yes',
    security: 'P-7',
    users: '5+'
  },
  {
    model: '41534',
    category: 'High Security',
    frequency: 'daily',
    capacity: '8 / 10',
    cleantec: 'yes',
    security: 'P-7',
    users: '5+'
  },
  {
    model: '50214',
    category: 'Small Office',
    frequency: 'daily',
    capacity: '10 / 12',
    cleantec: 'no',
    security: 'P-4',
    users: '3'
  },
  {
    model: '50314',
    category: 'Small Office',
    frequency: 'daily',
    capacity: '16 / 18',
    cleantec: 'no',
    security: 'P-4',
    users: '3'
  },
  {
    model: '50414',
    category: 'Office',
    frequency: 'daily',
    capacity: '18 / 20',
    cleantec: 'no',
    security: 'P-4',
    users: '5'
  },
  {
    model: '50464',
    category: 'Office',
    frequency: 'daily',
    capacity: '24 / 26',
    cleantec: 'no',
    security: 'P-4',
    users: '5'
  },
  {
    model: '50564',
    category: 'Department',
    frequency: 'daily',
    capacity: '22 / 24',
    cleantec: 'no',
    security: 'P-4',
    users: '5+'
  },
  {
    model: '20434ds',
    category: 'High Security',
    frequency: 'daily',
    capacity: '4 / 6',
    cleantec: 'no',
    security: 'P-7',
    users: '5+'
  },
  {
    model: '22022',
    category: 'Deskside',
    frequency: 'daily',
    capacity: '7 / 9',
    cleantec: 'no',
    security: 'P-4',
    users: '1'
  },
  {
    model: '22092',
    category: 'Deskside',
    frequency: 'daily',
    capacity: '10 / 12',
    cleantec: 'no',
    security: 'P-4',
    users: '1'
  },
  {
    model: '22312',
    category: 'Deskside',
    frequency: 'daily',
    capacity: '15 / 17',
    cleantec: 'no',
    security: 'P-4',
    users: '1'
  },
  {
    model: '22318',
    category: 'Deskside',
    frequency: 'daily',
    capacity: '16 / 18',
    cleantec: 'no',
    security: 'P-4',
    users: '1'
  },
  {
    model: '50114',
    category: 'Small Office',
    frequency: 'daily',
    capacity: '10 / 12',
    cleantec: 'no',
    security: 'P-4',
    users: '3'
  },
  {
    model: '51214',
    category: 'Small Office',
    frequency: 'daily',
    capacity: '10 / 12',
    cleantec: 'no',
    security: 'P-4',
    users: '3'
  },
  {
    model: '50310',
    category: 'Small Office',
    frequency: 'daily',
    capacity: '20 / 22',
    cleantec: 'no',
    security: 'P-3',
    users: '3'
  },
  {
    model: '51314',
    category: 'Small Office',
    frequency: 'daily',
    capacity: '16 / 18',
    cleantec: 'no',
    security: 'P-4',
    users: '3'
  },
  {
    model: '51322',
    category: 'Small Office',
    frequency: 'daily',
    capacity: '12 / 14',
    cleantec: 'yes',
    security: 'P-5',
    users: '3'
  },
  {
    model: '50410',
    category: 'Office',
    frequency: 'daily',
    capacity: '20 / 22',
    cleantec: 'no',
    security: 'P-3',
    users: '5'
  },
  {
    model: '51414',
    category: 'Office',
    frequency: 'daily',
    capacity: '18 / 20',
    cleantec: 'no',
    security: 'P-4',
    users: '5'
  },
  {
    model: '51422',
    category: 'Office',
    frequency: 'daily',
    capacity: '13 / 15',
    cleantec: 'yes',
    security: 'P-5',
    users: '5'
  },
  {
    model: '51464',
    category: 'Office',
    frequency: 'daily',
    capacity: '24 / 26',
    cleantec: 'no',
    security: 'P-4',
    users: '5'
  },
  {
    model: '51472',
    category: 'Office',
    frequency: 'daily',
    capacity: '16 / 18',
    cleantec: 'yes',
    security: 'P-5',
    users: '5'
  },
  {
    model: '51514',
    category: 'Department',
    frequency: 'daily',
    capacity: '26 / 28',
    cleantec: 'no',
    security: 'P-4',
    users: '5+'
  },
  {
    model: '51522',
    category: 'Department',
    frequency: 'daily',
    capacity: '16 / 18',
    cleantec: 'yes',
    security: 'P-5',
    users: '5+'
  },
  {
    model: '51564',
    category: 'Department',
    frequency: 'daily',
    capacity: '22 / 24',
    cleantec: 'no',
    security: 'P-4',
    users: '5+'
  },
  {
    model: '51572',
    category: 'Department',
    frequency: 'daily',
    capacity: '14 / 16',
    cleantec: 'yes',
    security: 'P-5',
    users: '5+'
  },
  {
    model: '50514',
    category: 'Department',
    frequency: 'daily',
    capacity: '26 / 28',
    cleantec: 'no',
    security: 'P-4',
    users: '5+'
  },
  {
    model: '707 PS',
    category: 'High Security',
    frequency: 'daily',
    capacity: '11 / 13',
    cleantec: 'no',
    security: 'P-7',
    users: '5+'
  },
  {
    model: '717 OS',
    category: 'Optical Shredder',
    frequency: 'daily',
    capacity: '11 / 13',
    cleantec: 'no',
    security: 'O-5',
    users: '5+'
  },
  {
    model: '727 CS',
    category: 'High Security',
    frequency: 'daily',
    capacity: '11 / 13',
    cleantec: 'no',
    security: 'P-7 | O-5',
    users: '5+'
  },
  {
    model: '808 MS',
    category: 'Media Shredder',
    frequency: 'daily',
    capacity: '11 / 13',
    cleantec: 'no',
    security: 'O-3 | E-3',
    users: '5+'
  },
  {
    model: '818 HD',
    category: 'Hard Drive Punch',
    frequency: 'daily',
    capacity: '11 / 13',
    cleantec: 'no',
    security: 'H-3',
    users: '5+'
  },
  {
    model: '828 HD',
    category: 'Hard Drive Shredder',
    frequency: 'daily',
    capacity: '100 / 110',
    cleantec: 'no',
    security: 'P-3 | H-4',
    users: '5+'
  },
  {
    model: '909 HS',
    category: 'High Capacity',
    frequency: 'daily',
    capacity: '85 / 90',
    cleantec: 'no',
    security: 'P-3',
    users: '5+'
  },
  {
    model: '919 IS',
    category: 'High Capacity',
    frequency: 'daily',
    capacity: '85 / 90',
    cleantec: 'no',
    security: 'P-3',
    users: '5+'
  },
  {
    model: '929 IS',
    category: 'High Capacity',
    frequency: 'daily',
    capacity: '500',
    cleantec: 'no',
    security: 'P-3',
    users: '5+'
  },
]