import Capacities from "@/config/Capacities";
import Categories from "@/config/Categories";

export default function () {
  const capacityNumbers = {};
  capacityNumbers[ Capacities.LESS_THAN_1k ] = 0;
  capacityNumbers[ Capacities.FROM_1k_4K ] = 1;
  capacityNumbers[ Capacities.FROM_4K_8K ] = 2;
  capacityNumbers[ Capacities.FROM_8K_12K ] = 3;
  capacityNumbers[ Capacities.MORE_THAN_12K ] = 4;



  const categoryCapacities = {};

  //Less than 1k:
  categoryCapacities[Categories.DESKSIDE] = [Capacities.LESS_THAN_1k];
  categoryCapacities[Categories.SMALL_OFFICE] = [Capacities.LESS_THAN_1k];

  // 1k-4k, and 4k-8k:
  categoryCapacities[Categories.OFFICE] = [Capacities.FROM_1k_4K, Capacities.FROM_4K_8K];
  categoryCapacities[Categories.DEPARTMENT] = [Capacities.FROM_1k_4K, Capacities.FROM_4K_8K];


  // 8k-12k, and 12k+
  categoryCapacities[Categories.HIGH_CAPACITY] = [Capacities.FROM_8K_12K, Capacities.MORE_THAN_12K];
  categoryCapacities[Categories.HIGH_SECURITY] = [Capacities.FROM_8K_12K, Capacities.MORE_THAN_12K];
  categoryCapacities[Categories.OPTICAL] = [Capacities.FROM_8K_12K, Capacities.MORE_THAN_12K];
  categoryCapacities[Categories.MEDIA] = [Capacities.FROM_8K_12K, Capacities.MORE_THAN_12K];
  categoryCapacities[Categories.HD_PUNCH] = [Capacities.FROM_8K_12K, Capacities.MORE_THAN_12K];
  categoryCapacities[Categories.HD_SHREDDER] = [Capacities.FROM_8K_12K, Capacities.MORE_THAN_12K];


  this.score = function(shredder, capacity) {

    //Get an array of the capacities which are maapped to the category of the shredder.
    const shredderCapacities = categoryCapacities[shredder.category];


    //Get the distance between the numeric value of the capacity & the numeric value of each of the
    //capacities mapped to the shredder.
    const distances = shredderCapacities.map(loc =>  {
      const numericVal = capacityNumbers[loc];
      return Math.abs(numericVal - capacityNumbers[capacity]);
    });


    const lowestDistance = distances.sort()[0];

    //If 0, final score is 1 to indicate perfect match, otherwise multiply it by -1 to convert the biggest distance
    //into the smallest score
    return (lowestDistance === 0) ? 1 : lowestDistance * -1;
  };

};