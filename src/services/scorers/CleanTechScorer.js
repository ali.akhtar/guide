export default function () {
  this.score = function(shredder, cleanTech) {
    if (cleanTech === 'yes' && shredder.cleantec === 'yes')
      return 6; //if they picked yes, give a high weight to all shredders with cleantec = yes


    if (shredder.cleantec === cleanTech)
      return 1;
    else
      return -1;
  };

};