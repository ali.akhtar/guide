import Users from "@/config/Users";

export default function () {
  const userNumbers = {};
  userNumbers[ Users.ONE ] = 0;
  userNumbers[ Users.ONE_TO_THREE ] = 1;
  userNumbers[ Users.THREE_TO_FIVE ] = 2;
  userNumbers[ Users.FIVE_PLUS ] = 3;

  this.score = function(shredder, users) {
    const shredderUsersIndex = userNumbers[shredder.users];
    const distance = Math.abs( shredderUsersIndex - userNumbers[users] );
    if (distance === 0)
      return 1;
    else
      return  distance * -1;
  };

};