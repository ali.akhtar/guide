import Locations from "@/config/Locations";
import Categories from "@/config/Categories";

export default function () {


  //Numeric values used to determine how similar the locations are to each other - e.g home is closer to business than
  //it is to gov, and vice versa.
  const locationNumericValues = {};
  locationNumericValues[ Locations.HOME ] = 0;
  locationNumericValues[ Locations.BIZ ] = 1;
  locationNumericValues [ Locations.GOV ] = 2;


  const categoryLocations = {};

  //Categories for home:
  categoryLocations[Categories.DESKSIDE] = [Locations.HOME];

  //Categories for both home & office:
  categoryLocations[Categories.SMALL_OFFICE] = [Locations.HOME, Locations.BIZ];

  //Categories for business:
  categoryLocations[Categories.OFFICE] = [Locations.BIZ];

  //Categories for both business & gov:
  categoryLocations[Categories.DEPARTMENT] = [Locations.BIZ, Locations.GOV];
  categoryLocations[Categories.HD_PUNCH] = [Locations.BIZ, Locations.GOV];
  categoryLocations[Categories.HD_SHREDDER] = [Locations.BIZ, Locations.GOV];
  categoryLocations[Categories.MEDIA] = [Locations.BIZ, Locations.GOV];
  categoryLocations[Categories.OPTICAL] = [Locations.BIZ, Locations.GOV];


  //Categories for gov:
  categoryLocations[Categories.HIGH_CAPACITY] = [Locations.GOV];
  categoryLocations[Categories.HIGH_SECURITY] = [Locations.GOV];



  /**
   * Returns a numeric value for how close in similarity the location of a given shredder, and the user's
   * chosen location. The higher the value, the closer the match.
   *
   * Does this by looking up the difference in the numeric values of the given location and the locations for
   * the category of the shredder. E.g if location = home and shredder category = deskside, then its location will
   * also be home. Home has a numeric value of 0, so 0 - 0 = 0. But if user had chosen gov, then it would've been
   * 0 - 2 and the distance would've been 2.
   *
   * If the score is 0, it means a perfect match, so 1 is returned in its place.
   *
   * Otherwise, The final score is multiplied by -1 to turn positive distances to negative, so the score would be low to
   * indicate a lower match.
   */
  this.score = function(shredder, location) {

    //Get an array of the locations which are maapped to the category of the shredder.
    const shredderLocations = categoryLocations[shredder.category];


    //Get the distance between the numeric value of the location & the numeric value of each of the
    //locations mapped to the shredder.
    const locationDistances = shredderLocations.map(loc =>  {
      const numericVal = locationNumericValues[loc];
      return Math.abs(numericVal - locationNumericValues[location]);
    });

    //Add a small penalty if there were multiple options for this category, e.g so 'deskside' is put slightly
    //higher than 'small office' for the 'home' location, and so on.
    const dilutionPenalty = (locationDistances.length > 1) ? 0.1 : 0.0;

    const lowestDistance = locationDistances.sort()[0] + dilutionPenalty; //pick the lowest distance

    //If 0, final score is 1 to indicate perfect match, otherwise multiply it by -1 to convert the biggest distance
    //into the smallest score
    return (lowestDistance === 0) ? 1 : lowestDistance * -1;
  };

}