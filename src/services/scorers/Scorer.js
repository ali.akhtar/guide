import LocationScorer from "@/services/scorers/LocationScorer";
import CapacityScorer from "@/services/scorers/CapacityScorer";
import UserScorer from "@/services/scorers/UserScorer";
import CleanTechScorer from "@/services/scorers/CleanTechScorer";
import SecurityScorer from "@/services/scorers/SecurityScorer";

export default function () {

  const that = this;
  const numberOfScores = 6.0; //how many different scores are there - basically the number of questions


  const locationScorer = new LocationScorer();
  const capacityScorer = new CapacityScorer();
  const usersScorer = new UserScorer();
  const cleanTechScorer = new CleanTechScorer();
  const securityScorer = new SecurityScorer();

  this.locationScore = function(shredder, location) {
    return locationScorer.score(shredder, location);
  };

  this.capacityScore = function(shredder, capacity) {
    return capacityScorer.score(shredder, capacity);
  };

  this.usersScore = function(shredder, users) {
    return usersScorer.score(shredder, users);
  };


  this.cleanTechScore = function(shredder, cleanTech) {
    return cleanTechScorer.score(shredder, cleanTech);
  };

  this.securityScore = function(shredder, security) {
    return securityScorer.score(shredder, security);
  };



  this.overallScore = function(shredder) {

    const scores = shredder.scores;

    const scoresSum = scores.location + scores.frequency + scores.capacity + scores.users + scores.cleanTech + scores.security;

    if (scoresSum === 0)
      return 0; //avoid divison by zero

    return scoresSum / numberOfScores;
  };

}