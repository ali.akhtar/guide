import Security from "@/config/Security";

export default function () {
  const securityNumbers = {};
  securityNumbers[ Security.P1 ] = 0;
  securityNumbers[ Security.P2 ] = 1;
  securityNumbers[ Security.P3 ] = 2;
  securityNumbers[ Security.P4 ] = 3;
  securityNumbers[ Security.P5 ] = 4;
  securityNumbers[ Security.P6 ] = 5;
  securityNumbers[ Security.P7 ] = 7;

  const shredderSecurityMappings = {};
  shredderSecurityMappings['P-1'] = Security.P1;
  shredderSecurityMappings['P-2'] = Security.P2;
  shredderSecurityMappings['P-3'] = Security.P3;
  shredderSecurityMappings['P-4'] = Security.P4;
  shredderSecurityMappings['P-5'] = Security.P5;
  shredderSecurityMappings['P-6'] = Security.P6;
  shredderSecurityMappings['P-7'] = Security.P7;

  shredderSecurityMappings['P-7 | O-5'] = Security.P7;
  shredderSecurityMappings['O-5'] = Security.P7;

  shredderSecurityMappings['O-3 | E-3'] = Security.P5;
  shredderSecurityMappings['H-3'] = Security.P2;
  shredderSecurityMappings['P-3 | H-4'] = Security.P3;


  this.score = function(shredder, security) {
    const shredderSecurity = shredderSecurityMappings[shredder.security];
    if (typeof  shredderSecurity === 'undefined')
      window.console.error("No security mapping found for ", shredder.security);

    const shredderSecurityIndex = securityNumbers[shredderSecurity];
    const distance = Math.abs( shredderSecurityIndex - securityNumbers[security] );
    if (distance === 0)
      return 1;
    else
      return  distance * -1;
  };

};