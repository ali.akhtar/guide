import Vuex from 'vuex'
import Vue from 'vue';

import Search from "@/services/Search";

Vue.use(Vuex);

export default new Vuex.Store({

  modules: {
    search: Search
  },

  state: {
    currentQuestion: 0,
    swiper: null
  },

  getters: {
    swiper(state) {
      return state.swiper;
    },

    isIntro(state) {
      return state.currentQuestion === 0;
    },

    isResults(state) {
      return state.currentQuestion === 7;
    },

    isQuestion: (state) => (question) => state.currentQuestion === question,

  },

  mutations: {
    setSwiper(state, swiper) {
      state.swiper = swiper;
    },

    goto(state, question) {
      state.currentQuestion = question;
      this.getters.swiper.slideTo(question, 800, '');
      this.dispatch('setSeen');
    },


  },

  actions: {
    setSeen() {
      const slideActive = document.querySelector('.swiper-pagination-bullet-active');
      const slideSeen   = ' was-seen';
      if (slideActive.className.indexOf(slideSeen) === -1) {
        slideActive.className += slideSeen;
      }
    },

  }
})