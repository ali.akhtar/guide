const Locations = {
  HOME: "home",
  BIZ: "biz",
  GOV: "gov"
};

export default Object.freeze(Locations);