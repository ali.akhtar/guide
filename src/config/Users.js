const Users = {
  ONE: "1",
  ONE_TO_THREE: "3",
  THREE_TO_FIVE: "5",
  FIVE_PLUS: "5+"
};

export default Object.freeze(Users);