const Capacities = {
  LESS_THAN_1k: "0-1k",
  FROM_1k_4K: "1k-4k",
  FROM_4K_8K: "4k-8k",
  FROM_8K_12K: "8k-12k",
  MORE_THAN_12K: "12k+"
};

export default Object.freeze(Capacities);