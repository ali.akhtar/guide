const Categories = {

  HIGH_CAPACITY: "High Capacity",
  HIGH_SECURITY: "High Security",
  SMALL_OFFICE: "Small Office",
  OFFICE: "Office",
  DEPARTMENT: "Department",
  DESKSIDE: "Deskside",
  OPTICAL: "Optical Shredder",
  MEDIA: "Media Shredder",
  HD_PUNCH: "Hard Drive Punch",
  HD_SHREDDER: "Hard Drive Shredder"


};

export default Object.freeze(Categories);