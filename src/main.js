import Vue from 'vue'
import Vuex from 'vuex'

import store from "./services/Store.js"
import App from './App.vue'


Vue.config.productionTip = false;


new Vue({
  store,
  render: h => h(App),
}).$mount('#app');
